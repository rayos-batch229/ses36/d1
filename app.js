// 
const express = require("express");
const mongoose = require("mongoose");
const taskRoutes = require("./routes/taskRoutes.js");

// Server Setup

const app = express();
const port = 4001;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Add the tasks route
// Allows all the task routes created in the taskRoutes.js 
app.use("/tasks", taskRoutes);
app.use("/users", taskRoutes);

mongoose.connect("mongodb+srv://admin:admin1234@cluster0.s99ywbq.mongodb.net/B229_to-do?retryWrites=true&w=majority",{
	useNewUrlParser : true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database."));

app.listen(port, () => console.log(`Now listening to port ${port}.`));